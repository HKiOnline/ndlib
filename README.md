# ndlib
Narrative Dice library for FFG Star Wars Roleplaying Game.
Pass the dice you which to roll as flag arguments to the main Roll()-function.


    usage: ndclient [<flags>]

    Flags:
        --help                Show context-sensitive help (also try --help-long and --help-man).
    -a, --ability=0           Number of Ability-dice in the roll
    -p, --proficiency=0       Number of Proficiency-dice in the roll
    -b, --boost=0             Number of Boost-dice in the roll
    -d, --difficulty=0        Number of Difficulty-dice in the roll
    -c, --challenge=0         Number of Challenge-dice in the roll
    -s, --setback=0           Number of Setback-dice in the roll
    -f, --force=0             Number of Force-dice in the roll
    -%, --percentile          Roll Percentile-dice
    -o, --percentileOffset=0  Set percentile offset


Input: give the dice to be thrown
Output: resolve throw result

## Dices:
The library supports all of the narrative dices including the percentile dice.

- Ability (eight sided dice)
- Profiency (twelve sided dice)
- Boost (six sided dice)
- Difficulty (eight sided dice)
- Challenge (twelve sided dice)
- Force (twelve sided dice)
- Percentile (two ten sided dices, normal rand will do)

## Results:
The library supports all of the narrative dice symbols.

- success
- advantage
- triumph
- failure
- threat
- despair
- light side point
- dark side point
- numeric (1 - 100)
