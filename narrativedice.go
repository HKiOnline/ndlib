// Narrative Dice Generator for FFG Star Wars Roleplaying Game.
//
// Input: give the dice to be thrown
// Output: resolve throw result
//
// Dices:
// - (a)bility (eight sided dice)
// - (p)rofiency (twelve sided dice)
// - (b)oost (six sided dice)
// - (d)ifficulty (eight sided dice)
// - (c)hallenge (twelve sided dice)
// - (f)orce (twelve sided dice)
// - (per)centile (two ten sided dices, normal rand will do)
//
// Results:
// - success
// - advantage
// - triumph
// - failure
// - threat
// - despair
// - light side point
// - dark side point
// - numeric (1 - 100)

package narrativedice

import (
    "math/rand"
    "time"
    "fmt"
)

type Dicepool struct {
  Ability int
  Proficiency int
  Boost int
  Difficulty int
  Challenge int
  Setback int
  Force int
  Percentile bool
  PercentileOffset int
}

type Results struct {
  Success int
  Advantage int
  Triumph int
  Failure int
  Threat int
  Despair int
  Lspoint int
  Dspoint int
  Percentile int
}

func Roll(pool Dicepool) Results {

  // Set default values
  abSuccess, abAdvantage, profSuccess, profAdvantage, triumph, boostSuccess, boostAdvantage := 0,0,0,0,0,0,0
  difFailure, difThreat, chalFailure, chalThreat, despair, setFailure, setThreat := 0,0,0,0,0,0,0
  lsforcepoints, dsforcepoints, percentileResult := 0,0,0
  resultMatrix := Results{}

  // Check if we have more than one of the dice type, then roll

  if pool.Ability > 0 {
    abSuccess, abAdvantage = rollAbilityDice(pool.Ability)
  }

  if pool.Proficiency > 0 {
    profSuccess, profAdvantage, triumph = rollProficiencyDice(pool.Proficiency)
  }

  if pool.Boost > 0 {
    boostSuccess, boostAdvantage = rollBoostDice(pool.Boost)
  }

  if pool.Difficulty > 0 {
    difFailure, difThreat = rollDificultyDice(pool.Difficulty)
  }

  if pool.Challenge > 0 {
    chalFailure, chalThreat, despair = rollChallengeDice(pool.Challenge)
  }

  if pool.Setback > 0 {
    setFailure, setThreat = rollSetbackDice(pool.Setback)
  }

  if pool.Force > 0 {
    lsforcepoints, dsforcepoints = rollForceDice(pool.Force)
  }

  if pool.Percentile {
    percentileResult = rollPercentile(pool.PercentileOffset)
  }

  // Positive dice result tally
  resultMatrix.Success = abSuccess + profSuccess + boostSuccess + triumph
  resultMatrix.Advantage = abAdvantage + profAdvantage + boostAdvantage
  resultMatrix.Triumph = triumph

  // Negative dice result tally
  resultMatrix.Failure = difFailure + chalFailure + setFailure + despair
  resultMatrix.Threat = difThreat + chalThreat + setThreat
  resultMatrix.Despair = despair

  // Using the Force
  resultMatrix.Lspoint = lsforcepoints
  resultMatrix.Dspoint = dsforcepoints

  // percentile result
  resultMatrix.Percentile = percentileResult

  return resultMatrix
}

func TextSummary(results Results) string {

  success := results.Success - results.Failure
  advantage := results.Advantage - results.Threat
  summaryText := ""

  if success > 0 {
    summaryText = fmt.Sprintf("• %d sucesses", success)
  } else if success == 0 {
    summaryText = "• no successes or failures"
  } else {
    failures := success * -1
    summaryText = fmt.Sprintf("• %d failures", failures)
  }

  if advantage > 0 {
    summaryText += fmt.Sprintf("\n• %d advantages", advantage)
  } else if advantage == 0 {
    summaryText += "\n• no advantages or threats"
  } else {
    threats := advantage * -1
    summaryText += fmt.Sprintf("\n• %d threats", threats)
  }

  if results.Triumph > 0 {
    summaryText += fmt.Sprintf("\n• %d triumphs", results.Triumph)
  }

  if results.Despair > 0 {
    summaryText += fmt.Sprintf("\n• %d despairs", results.Despair)
  }

  if results.Lspoint > 0 {
    summaryText += fmt.Sprintf("\n• %d light side points", results.Lspoint)
  }

  if results.Dspoint > 0 {
    summaryText += fmt.Sprintf("\n• %d dark side points", results.Dspoint)
  }

  if (results.Percentile > 0){
    summaryText += fmt.Sprintf("\n• %d numeric result", results.Percentile)
  }

  return summaryText
}

func rollPercentile(offset int) int {
  return getDiceNumber(1, 100, offset)
}

func rollAbilityDice(dice int) (int, int) {

  success := 0
  advantage := 0

  for i := 0; i < dice; i++ {
    diceNumber := getDiceNumber(1, 8, 0)

    switch diceNumber {
    case 2, 3:
      success = success + 1
    case 4:
      success = success + 2
    case 5, 6:
      advantage = advantage + 1
    case 7:
      success = success + 1
      advantage = advantage + 1
    case 8:
      advantage = advantage + 2
    }
  }

  return success, advantage
}

func rollProficiencyDice(dice int) (int, int, int) {

  success := 0
  advantage := 0
  triumph := 0

  for i := 0; i < dice; i++ {

    diceNumber := getDiceNumber(1, 12, 0)

    switch diceNumber {
    case 2, 3:
      success = success + 1
    case 4, 5:
      success = success + 2
    case 6:
      advantage = advantage + 1
    case 7, 8, 9:
      success = success + 1
      advantage = advantage + 1
    case 10, 11:
      advantage = advantage + 2
    case 12:
      triumph = triumph + 1
    }
  }

  return success, advantage, triumph
}

func rollBoostDice(dice int) (int, int) {

  success := 0
  advantage := 0

  for i := 0; i < dice; i++ {

    diceNumber := getDiceNumber(1, 6, 0)

    switch diceNumber {
    case 3:
      success = success + 1
    case 4:
      success = success + 1
      advantage = advantage + 1
    case 5:
      advantage = advantage + 2
    case 6:
      advantage = advantage + 1
    }
  }

  return success, advantage
}

func rollDificultyDice(dice int) (int, int) {

  failure := 0
  threat := 0

  for i := 0; i < dice; i++ {
    diceNumber := getDiceNumber(1, 8, 0)

    switch diceNumber {
    case 2:
      failure = failure + 1
    case 3:
      failure = failure + 2
    case 4, 5, 6:
      threat = threat + 1
    case 7:
      threat = threat + 2
    case 8:
      failure = failure + 1
      threat = threat + 1
    }
  }

  return failure, threat
}

func rollChallengeDice(dice int) (int, int, int) {

  failure := 0
  threat := 0
  despair := 0

  for i := 0; i < dice; i++ {

    diceNumber := getDiceNumber(1, 12, 0)

    switch diceNumber {
    case 2, 3:
      failure = failure + 1
    case 4, 5:
      failure = failure + 2
    case 6:
      threat = threat + 1
    case 7, 8, 9:
      failure = failure + 1
      threat = threat + 1
    case 10, 11:
      threat = threat + 2
    case 12:
      despair = despair + 1
    }
  }

  return failure, threat, despair
}

func rollSetbackDice(dice int) (int, int) {

  failure := 0
  threat := 0

  for i := 0; i < dice; i++ {

    diceNumber := getDiceNumber(1, 6, 0)

    switch diceNumber {
    case 3, 4:
      failure = failure + 1
    case 5, 6:
      threat = threat + 1
    }
  }

  return failure, threat
}

func rollForceDice(dice int) (int, int) {

  darkSidePoints := 0
  lightSidePoints := 0

  for i := 0; i < dice; i++ {

    diceNumber := getDiceNumber(1, 12, 0)

    switch diceNumber {
    case 1,2,3,4,5,6:
      darkSidePoints = darkSidePoints + 1
    case 7:
      darkSidePoints = darkSidePoints + 2
    case 8, 9:
      lightSidePoints = lightSidePoints + 1
    case 10, 11,12:
      lightSidePoints = lightSidePoints + 2
    }
  }

  return lightSidePoints, darkSidePoints
}


func getDiceNumber(min int, max int, offset int) int {

  seed := rand.NewSource(time.Now().UnixNano())
  number := rand.New(seed)
  result := number.Intn(max+1) + offset

  if result > max {
    return max
  }

  if result < min {
    return min
  }

  return result
}
